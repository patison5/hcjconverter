function Render (container, leftArticle, result) {
	var that = this;
	that.container = document.getElementsByClassName(container)[0];
	that.containerWidth = window.getComputedStyle(that.container).width; 
	that.containerHeight = window.getComputedStyle(that.container).height; 

	// левая колонка
	that.leftArticle = document.getElementsByClassName(leftArticle)[0];
	that.leftArticleWidth = window.getComputedStyle(that.leftArticle).width;
	that.leftArticleHeight = window.getComputedStyle(that.leftArticle).height;

	// обвертка окна вывода
	that.result = document.getElementsByClassName(result)[0];

	//поля ввода (textarea)
	var block0 = document.getElementsByClassName('block')[0],
		block1 = document.getElementsByClassName('block')[1],
		block2 = document.getElementsByClassName('block')[2],
		block0Display = block0.display,
		block1Display = block1.display,
		block2Display = block2.display;

		console.log(block0)  // выводит
		console.log(block1)  // выводит
		console.log(block2)  // выводит

		console.log(block0Display)  // ничего не выводит!
		console.log(block1Display)  // ничего не выводит!
		console.log(block2Display)  // ничего не выводит!

	// кнопки открыть/закрыть
	var open0 = document.getElementsByClassName('open')[0],
		open1 = document.getElementsByClassName('open')[1],
		open2 = document.getElementsByClassName('open')[2];

	var top_line = document.getElementsByClassName('top_line')[0];
		top_lineHeight = window.getComputedStyle(top_line).height;

	// Кнопки Clear
	var clearHTML = document.getElementById('clearHTML'),
		clearCSS = document.getElementById('clearCSS'),
		clearJS = document.getElementById('clearJS'),
		clearAll = document.getElementById('clearAll');

	console.log(clearHTML,clearCSS,clearJS)

	// основная функция
	that.renderResult_ = function () {

		// блок с заголовком
		var title = document.getElementsByClassName('title')[0],
			titleHeight = window.getComputedStyle(title).height; 

		// высота/длинна обверки окна вывода
		that.result.style.width = (parseInt(that.containerWidth) - parseInt(that.leftArticleWidth)) + 'px';
		that.result.style.height = (parseInt(that.containerHeight) - parseInt(top_lineHeight)) + 'px';

		that.leftArticle.style.height = parseInt(that.containerHeight) - parseInt(top_lineHeight) + 'px';

		// изменение высоты блоков
		block0.style.height = (parseInt(that.leftArticle.style.height) - parseInt(titleHeight) * 3) / 3 + 'px';
		block1.style.height = block0.style.height;
		block2.style.height = block0.style.height;


		// изменение высоты трех окон ввода
		function RezizeBlock () {
			
			var block0Display = block0.display,
				block1Display = block1.display,
				block2Display = block2.display;

			console.log(block0Display)
			console.log(block1Display)
			console.log(block2Display)

			if (block0Display == 'none') {
				block1.style.height = parseInt(that.containerHeight) / 2 + 'px';
				block2.style.height = block1.style.height; 

				console.log(block1.style.height)
			} else if (block1Display == 'none') {
				block0.style.height = parseInt(that.containerHeight) / 2 + 'px';
				block2.style.height = block1.style.height;

				console.log(block0.style.height)
			} else if (block2Display == 'none') {
				block0.style.height = parseInt(that.containerHeight) / 2 + 'px';
				block1.style.height = block1.style.height;

				console.log(block0.style.height)
			} else if ((block0Display == 'none') && (block1Display == 'none')) {
				block2.style.height = parseInt(that.containerHeight) + 'px';
				console.log(block2.style.height)
			} else if ((block1Display == 'none') && (block2Display == 'none')) {
				block0.style.height = parseInt(that.containerHeight) + 'px';
				console.log(block0.style.height)
			} else if ((block0Display == 'none') && (block2Display == 'none')) {
				block1.style.height = parseInt(that.containerHeight) + 'px';
				console.log(block1.style.height)
			}
		}

		// открыть/скрыть блок
		function OpenCLose (openEl) {
			var textarea = (openEl.parentNode).parentNode.childNodes[3];
				textareaDisplay = window.getComputedStyle(textarea).display;

			if (textareaDisplay == 'block') {
				textarea.style.display = 'none';
			} else {
				textarea.style.display = 'block';
			}
			console.log(textarea.style.display)
		}

		open0.addEventListener("click", function () { OpenCLose(open0); RezizeBlock(); });
		open1.addEventListener("click", function () { OpenCLose(open1); RezizeBlock(); });
		open2.addEventListener("click", function () { OpenCLose(open2); RezizeBlock(); });

		function deleteAll (id) {
			if (id == "clearHTML") {
				block0.value = "";
			} else if (id == "clearCSS") {
				block1.value = "";
			} else if (id == "clearJS") {
				block2.value = "";
			} else {
				block0.value = "";
				block1.value = "";
				block2.value = "";
			}

		}

		clearHTML.addEventListener("click", function () { deleteAll("clearHTML");});
		clearCSS.addEventListener("click", function () { deleteAll("clearCSS");});
		clearJS.addEventListener("click", function () { deleteAll("clearJS");});
		clearAll.addEventListener("click", function () { deleteAll("");});
		
		
	// сохранение в lochalStorage
	var SaveBtn = document.getElementsByClassName('save')[0];
	function saveToStorage () {
		var htmlValue = document.getElementsByClassName('block')[0],
			cssValue = document.getElementsByClassName('block')[1],
			jsValue = document.getElementsByClassName('block')[2],
			result = document.getElementsByClassName('result')[0];

		
		localStorage.setItem("htmlValue", htmlValue.value);
		localStorage.setItem("cssValue", cssValue.value);
		localStorage.setItem("jsValue", jsValue.value);
		
		htmlValue.value = localStorage.getItem("htmlValue");
		cssValue.value = localStorage.getItem("cssValue");
		jsValue.value = localStorage.getItem("jsValue");
	}
	
	SaveBtn.addEventListener("click", function () { saveToStorage(); });
	
	
	
	// при обновлении страницы будет произведена запись значений из lochalStorage
	var htmlValue = document.getElementsByClassName('block')[0],
			cssValue = document.getElementsByClassName('block')[1],
			jsValue = document.getElementsByClassName('block')[2],
			result = document.getElementsByClassName('result')[0];
			
		htmlValue.value = localStorage.getItem("htmlValue");
		cssValue.value = localStorage.getItem("cssValue");
		jsValue.value = localStorage.getItem("jsValue");
	// при обновлении страницы запись значений из lochalStorage
}

	that.init = function() {
		// отрисовываем результат
		that.renderResult_();
	}
}





window.onload = function () {
	var result = new Render("container", "leftArticle", "result");

	result.init();
}




//масштабирование элементов
window.onresize = function() {
	var that = this;
	result = document.getElementsByClassName("result")[0];

	container = document.getElementsByClassName("container")[0];
	containerWidth = window.getComputedStyle(container).width;

	leftArticle = document.getElementsByClassName("leftArticle")[0];
	that.leftArticleWidth = window.getComputedStyle(that.leftArticle).width;

	var block0 = document.getElementsByClassName('block')[0],
		block1 = document.getElementsByClassName('block')[1],
		block2 = document.getElementsByClassName('block')[2];

	result.style.width = (parseInt(that.containerWidth) - parseInt(that.leftArticleWidth)) + 'px';
	// console.log(result.style.width)
};



// var leftArticle = document.getElementsByClassName('leftArticle')[0],
		// leftArticleHeight = window.getComputedStyle(leftArticle).height;
		// console.log(leftArticleHeight)



// function startRendering () {
// 	console.log ('yeah')
// } 



function startRendering() {
	// Получить city и state из web-формы
	
	// setTimeout(function() { }, 500);
	
	var htmlValue = document.getElementsByClassName('block')[0].value,
		cssValue = document.getElementsByClassName('block')[1].value,
		jsValue = document.getElementsByClassName('block')[2].value
		result = document.getElementsByClassName('result')[0];
		


	// // Продолжать только если есть значения обоих полей
	// if ((htmlValue == null) || (htmlValue == "")) return;
	// if ((cssValue == null) || (cssValue == "")) return;
	// if ((jsValue == null) || (jsValue == "")) return;

	// // Создать URL для подключения
	// var url = "/scripts/getZipCode.php?htmlValue=" + escape(htmlValue) + "&cssValue=" + escape(cssValue) + "&jsValue=" + escape(jsValue);

	// // Открыть соединение с сервером
	// xmlHttp.open("post", url, true);

	// // Установить функцию для сервера, которая выполнится после его ответа
	// xmlHttp.onreadystatechange = updatePage;

	// // SПередать запрос
	// xmlHttp.send(null);

	$.ajax({
		type: "POST",
		url: "result.php",
		data: {
			htmlValue: htmlValue,
			cssValue: cssValue,
			jsValue: jsValue
		},
		// },
		complete: function (data) {
			var obj = eval(data);

			console.log(obj);
		}
	}).success(function($jsValue) {
		console.log($jsValue);
	})

	result.src = result.src;
}

// function updatePage() {
//   if (xmlHttp.readyState == 4) {
//     var response = xmlHttp.responseText;
//     document.getElementsByClassName("block")[2].value = response;
//   }
// }


// function createRequest () {
// 	var xmlHttp;
// }