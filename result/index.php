<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>HCJConverter</title>
	<link rel="stylesheet" href="css/style.css">
	<script src="js/script.js"></script>
	<script type="text/javascript" src="js/jquery-latest.js"></script>
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>



<!-- все,ч то ниже - не работающие стили подсветки синтаксиса -->
	<!-- Include required JS files -->
	<script type="text/javascript" src="js/shBrushXml.js"></script>
	 
	<!--
	    At least one brush, here we choose JS. You need to include a brush for every 
	    language you want to highlight
	-->
	<script type="text/javascript" src="js/shBrushJScript.js"></script>
	 
	<!-- Include *at least* the core style and default theme -->
	<link href="css/shCore.css" rel="stylesheet" type="text/css" />
	<link href="css/shThemeDefault.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="wraper">
		<div class="container clearfix">
			<div class="top_line">
				<div class="settings right">
					<span class="save buttoms">save</span>
					<span class="share buttoms">share</span>
					<span class="clearAll buttoms" id="clearAll">Clear All</span>
					<span class="Settings buttoms">Settings</span>
				</div>
			</div>
			<div class="leftArticle">
				<div class="html_block block-wrap">
					<div class="title clearfix">
						<h3>Html</h3>
						<i class="open">&nbsp;</i>
						<span class="clear" id="clearHTML">clear</span>
					</div>
					<div class="codeContainer">
						<textarea name="html_block" id="html_block" class="block" onkeyup="startRendering()"></textarea>
					</div>
				</div>
				<div class="css_block block-wrap">
					<div class="title clearfix">
						<h3>CSS</h3>
						<i class="open">&nbsp;</i>
						<span class="clear" id="clearCSS">clear</span>
					</div>
					<div class="codeContainer">
						<textarea name="css_block" id="css_block" class="block" onkeyup="startRendering()"></textarea>
					</div>
				</div>
				<div class="js_block block-wrap">
					<div class="title clearfix">
						<h3>JavaScript</h3>
						<i class="open">&nbsp;</i>
						<span class="clear" id="clearJS">clear</span>
					</div>
					<div class="codeContainer">
						<textarea name="js_block" id="js_block" class="block" onkeyup="startRendering()" name="name"></textarea>
					</div>
				</div>
			</div>

			<div class="right_content">
				<div>
					<iframe class="result" src="result/index.php"></iframe>
				</div>
			</div>
			<!-- <div class="result1"></div> -->
		</div>
	</div>

	<?

	$htmlVal = 'yeah',
	$cssValue = 'oooo',
	$jsValue =  $_POST['jsValue'];


	void echo ($jsValue);
	?>
</body>
</html>	